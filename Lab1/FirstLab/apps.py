from django.apps import AppConfig


class FirstlabConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'FirstLab'
